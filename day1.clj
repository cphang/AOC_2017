(require '[clojure.java.io :as io])
(require '[clojure.string :as string])

(def input ((comp string/trim slurp) "day1_input"))

(defn convert[d] (Character/digit d 10))

(defn digit_pair_match
  [a b]
  (if (== (convert a) (convert b))
    (convert a)
    0))
  
(def capcha_calculate_part1
  (reduce
    +
    (digit_pair_match (first input) (last input))
    (for [[a b] (partition 2 1 input)]
      (digit_pair_match a b))))

(def capcha_calculate_part2
  (* 2
    (reduce
      +
      (apply map digit_pair_match
             (partition (/ (count input) 2) input)))))

(println (str "Part 1: " capcha_calculate_part1))
(println (str "Part 2: " capcha_calculate_part2))
