#_(defdeps
      [[org.clojure/math.combinatorics "0.1.4"]])

(require '[clojure.java.io :as io])
(require '[clojure.string :as string])
(require '[clojure.math.combinatorics :as combo])

(defn to_integer[d] (Integer/parseInt d 10))

(defn sanitise_and_convert [line]
   (doall (map to_integer (#(string/split % #"\s+") line))))

(def input
  (with-open [rdr (io/reader "day2_input")]
    (doall (map sanitise_and_convert (line-seq rdr)))))

(def get_max
  (map (partial apply max) input))

(def get_min
  (map (partial apply min) input))

(def checksum
  (reduce + (map - get_max get_min)))

(def get_combinations
  (map #(combo/combinations % 2) input ))

(defn is_divisible
  [x y]
  (or 
    (zero? (mod x y))
    (zero? (mod y x))))

(def filter_combinations
  (apply concat
    (map 
      (partial filter (partial apply is_divisible)) 
      get_combinations)))

(def checksum_part_2
  (reduce +
          (map /
               (map (partial apply max) filter_combinations)
               (map (partial apply min) filter_combinations))))

(println checksum)
(println checksum_part_2)
